###########################################################################
## Make figures for BaseQTL paper 
#########################################################################

shell.prefix("source ~/.bashrc; ")


configfile: "config.yaml"

import pandas as pd
import subprocess
import os
import math              

from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider

HTTP = HTTPRemoteProvider()

localrules: all

# getting running time from log files
type_gt=["ngt", "gt"]

# power simulations
mean_keys=['1824', '5555']
mean_dic={k:[int(c)*-.1 for c in k] for k in mean_keys }
covs=[x/10 for x in range(6,11)]
effect_p=[0.6, 0.65,0.7]

figs=["Fig" + str(x) for x in range(2,7)]

# Download data files from https://doi.org/10.5281/zenodo.4748625

rule all:
    input:
        expand(config['source'] + "/{gtex}.allpairs.txt.gz", gtex=[ "Skin_Not_Sun_Exposed_Suprapubic", "Skin_Sun_Exposed_Lower_leg"]), # rule down_gtx
        expand(config['out_dir'] + "/sim/cov{cov}_mean{mean}_p{p}_GT.txt",  mean=mean_keys,cov=covs,p=effect_p), # rule power_simulation
        expand(config['out_dir'] + "/{fig}.png", fig=["Fig2bottom", "Fig3", "Fig4"]), # rule geu_figs
        expand(config['out_dir'] + "/{fig}.png", fig=["Fig5", "Fig6"]), # rule nor_pso
        config['out_dir'] + "/Fig2.pdf",
        config['out_dir'] + "/Fig3.pdf",
        config['out_dir'] + "/Fig4.pdf",
        config['out_dir'] + "/Fig5.pdf",
        config['out_dir'] + "/Fig6.pdf"


rule down_gtx:
    """ Downloads gtex data for skin cells, files too big to upoload in Zenodo."""
    input:
        HTTP.remote("storage.googleapis.com/gtex_analysis_v7/single_tissue_eqtl_data/all_snp_gene_associations/{gtex}.allpairs.txt.gz", keep_local=True)
    output:
        config['source'] + "/{gtex}.allpairs.txt.gz"
    run:
        outputName =os.path.join(config['source'] , os.path.basename(input[0]))
        shell("mv {input} {outputName} ")


        
rule power_simulation:
    """Simulates haplotypes and imputes the genotype for the cis-SNP at different info scores and then look at the power loss due to imputation. Number of total SNPs (1cis-SNP, rest fSNPs) in haplotype is len(mean)."""
    input:
        model_gt=config["source"] + "/neg.beta.prob.phasing.generalprior.eff2.stan" , # Stan model for GT from BaseQTL package
        model_nogt=config["source"] + "/neg.beta.noGT2.rsnp.generalprior.eff2.stan" # Stan model without genotypes from BaseQTL package
    params:
        N=1000,
        H=10000,
        S=100,
        n=100,
        eQTL_effect_p="{p}",
        mean=lambda wildcards: mean_dic[wildcards.mean],
        cov="{cov}",
        sim="cov{cov}_mean{mean}_p{p}",
        mu=500,
        negbinom_overdispersion=20,
        bb_overdispersion=5,
        frac_ase=0.1,
        out=config['out_dir'] + "/sim",
        prefix="cov{cov}_mean{mean}_p{p}"
    threads:
        16
    output:
        config['out_dir'] + "/sim/cov{cov}_mean{mean}_p{p}_GT.txt",
    script:
        "Scripts/sim_imp_power.R"
        
rule geu_figs:
    """Prepares main and supplementary figures and tables from GEUVADIS data. The input files, directories and other variables refer to the analysis done after applying the BaseQTL pipeline (gitLab), running BaseQTL, downloaded files or log files. Ouput figures as '.png' files and print tables in log file. Also saves input files required to run figures individually. The description of the input files is in the README file of this pipeline"""
    input:
        geneStEnd=config['source'] + "/gene_info.txt",
        sample_down_date=config['source'] + "/sample.downdate", 
        sample_info=config['source'] + "/GBR.samples",
        eSNPs=config['source'] + "/chr22.fSNPs.txt", 
        rna_dp=config['source'] + "/HG96_2215_chr22_q20.txt", 
        het_prop=config['source'] + "/fisher.fSNPs.txt", 
        dna=config['source'] + "/DNA.chr22.ASE.geuvadis_sub.vcf.gz", 
        rna=config['source'] + "/RNA.chr22.ASE.geuvadis_sub.vcf.gz", 
        AIgt=config['source'] + "/DNA_pre_post_AI.txt", 
        AIngt=config['source'] + "/RNA_pre_post_AI.txt",
        gt_fsnps_i=config['source'] + "/GT_fsnps_inference.rds",
        geu_sig=config['source'] + "/Geuvadis_sig_eqtl",
        rasqual=config['source'] + "/rasqual_fdr.rds" ,
        lm100KB=config['source'] + "/lm_fdr_100KB.rds",
        lm500KB=config['source'] + "/lm_fdr_500KB.rds",
        baseqtl=config['source'] + "/baseqtl_GT_RNA_NB.rds", 
        gt_noGT_wide=config['source'] + "/GT_RNA_wide.rds", 
        baseqtl_same=config['source'] + "/baseqtl_GT_RNA_sametags.rds", 
        baseqtl_nogt=config['source'] + "/baseqtl_noGT.rds",
        qc_cis=config['source'] + "/baseqtl_cisSNP_gt_error.rds",
        qc_f=config['source'] + "/baseqtl_fSNP_gt_error.rds" ,
        fprior=config['source'] + "/baseqtl_fprior.rds" , 
        run_time=expand(config['source'] + "/table_{type_gt}_comp_time.txt", type_gt=type_gt), 
        inp_GT=config['source'] + "/baseqtl_gt_input.rds", 
        gt_dprior=config['source'] + "/baseqtl_priors.rds",
        sim_power=expand(config['out_dir'] + "/sim/cov{cov}_mean{mean}_p{p}_GT.txt",  mean=mean_keys,cov=covs,p=effect_p)
    params:
        rbias=["norefbias", "rbias"], # prefix to distinguish BaseQTL runs with or without reference panel bias correction
        inf_prior=[0,0, 0.0309, 0.3479, 0.97359164, 0.02640836], # mean, sd and mixing proportion for default prior in BaseQTL,
        non_infprior=[0,10, 1], #mean and sd and mix for flat prior
        simdir=config['out_dir'] + "/sim", # directory with BaseQTL without genotypes simulations, output from rule power_simulation (above)
        prefix=expand("cov{cov}_mean{mean}_p{p}", mean=mean_keys,cov=covs,p=effect_p), # prefix for files in simdir output from rule power_simulation (above)
        
        out=config['out_dir']   # output directory to save files
    log:
        config['out_dir'] + "/geu_figs.log"
    output:
        expand(config['out_dir'] + "/{fig}.png", fig=["Fig2bottom", "Fig3", "Fig4"]) # some of the output figures, all will be created
    script:
        "Scripts/geu_final.R"
        
rule nor_pso:
    """ Prepare main and supplementary figures running normal and psoriasis eQTLs from RNA-seq. Output figres and source data to reproduce figures individually. The description of the input files is in the README file of this pipeline """
    input:
        gtexSigExp=config['source'] + "/Skin_Sun_Exposed_Lower_leg.v7.signif_variant_gene_pairs.txt.gz",
        gtexSigNoexp=config['source'] + "/Skin_Not_Sun_Exposed_Suprapubic.v7.signif_variant_gene_pairs.txt.gz", # file downloaded from GTex
        gtex_skin_all=expand(config['source'] + "/Skin_{skin}.allpairs.txt.gz", skin = ["Not_Sun_Exposed_Suprapubic", "Sun_Exposed_Lower_leg"]),
        drg=config['source'] + "/DRG_JID2014.xls",
        gwas=config['source'] + "/pso_gwasSuppTable2.csv",
        ind_model=config['source'] +  "/psoriasis_normal_eQTL.rds",
        joint=config['source']  + "/psoriasis_normal_joint.rds", 
        cor=config['source'] + "/fisher001EURr.rds",
        cor_cis=config['source'] + "/fisher001EURsmallcis_r.rds", 
        gwasr2=config['source'] + "/r2_spike_gwas.txt", 
        input_gene=config['source'] + "/psoriasis_normal_input.rds",
        input_all=config['source'] + "/psoriasis_normal_input_all.rds",
        lib_size=expand(config['source'] + "/{skin}_gc_lib_size.rds", skin = ["normal_skin", "Psoriasis_skin"]), 
    params:
        skin=["normal_skin", "Psoriasis_skin"], 
        genes2follow=["GSTP1", "PI3" , "SPRR1B"], # genes for main figure
        colclass=["character"]+["numeric" for i in range(7)], # column classess to open drg (xls file in input)
        out=config['out_dir'] # dir for output files
    log:
        config['out_dir'] + "/nor_pso_figs.log"
    output:
        expand(config['out_dir'] + "/{fig}.png", fig=["Fig5", "Fig6"]) # some of the output figures, all will be created
    script:
        "Scripts/nor_pso_final.R"


        
rule Figure_2:
    """This rules produces the data for Fig2b-d (Fig2a is a diagram)."""
    input:
        fig2b=config['source_fig'] + "/Fig2b.csv",
        fib2c=config['source_fig'] + "/Fig2c.csv",
        fig2d=config['source_fig'] + "/Fig2d.csv",
    output:
        out=config['out_dir'] + "/Fig2.pdf",
    script:
        "Scripts/Fig2.R"

rule Figure_3:
    """This rules produces the data for Fig3."""
    input:
        fig3a=config['source_fig'] + "/Fig3a.csv",
        fig3b=config['source_fig'] + "/Fig3b.csv",
        fig3c=config['source_fig'] + "/Fig3c.csv",
        fig3d=config['source_fig'] + "/Fig3d.csv",
    output:
        out=config['out_dir'] + "/Fig3.pdf",
    script:
        "Scripts/Fig3.R"

rule Figure_4:
    """This rules produces the data for Fig4. The file geneStEnd is needed to get start and end of genes around MAPK1 for the lowest panel in plot 4d."""
    input:
        fig4a=config['source_fig'] + "/Fig4a.csv",
        fig4b=config['source_fig'] + "/Fig4b.csv",
        fig4c=config['source_fig'] + "/Fig4c.rds",
        fig4d=config['source_fig'] + "/Fig4d.rds",
        geneStEnd=config['source'] + "/gene_info.txt", # file with start and end for genes, prepared in input pipeline
    output:
        out=config['out_dir'] + "/Fig4.pdf",
    script:
        "Scripts/Fig4.R"

rule Figure_5:
    """This rules produces the data for Fig5."""
    input:
        fig5a=config['source_fig'] + "/Fig5a.rds",
        fig5b=config['source_fig'] + "/Fig5b.csv",
    output:
        out=config['out_dir'] + "/Fig5.pdf",
    script:
        "Scripts/Fig5.R"

rule Figure_6:
    """This rules produces the data for Fig6."""
    input:
        fig6=config['source_fig'] + "/Fig6.rds",
    output:
        out=config['out_dir'] + "/Fig6.pdf",
    script:
        "Scripts/Fig6.R"
        
