
<!-- README.md is generated from README.Rmd. Please edit that file -->

# BaseQTL paper

This pipeline is designed for making the figures for the manuscript
describing [BaseQTL](https://gitlab.com/evigorito/baseqtl) an R pacakge
with a suite of models to discover eQTLs using allele specfic expresion
in a Bayesian framework. The pipeline assumes that the user has prepared
inputs from the GEUVADIS samples and psoriasis/normal skin samples
decribed in the paper (links to raw data in the paper).

The pipeline for makig inputs is described at
[input](https://gitlab.com/evigorito/baseqtl_pipeline/-/tree/master/input)
preparation. Starting from RNA-seq fastq files and a vcf file with
genotypes (when avaliable) the section describes how to prepare all
input files required for
[baseqtl](https://gitlab.com/evigorito/baseqtl).

The external data required to run this pipeline is deposited in
[Zenodo](https://doi.org/10.5281/zenodo.4759202)

## Instalation

Clone the repository

``` bash
# clone the baseqtl_paper repository
 git clone https://gitlab.com/evigorito/baseqtl_paper.git
 
```

## Workflow

We provide a
[snakemake](https://snakemake.readthedocs.io/en/stable/tutorial/tutorial.html)
to reproduce the figures and tables in the paper in a
[Snakefile](https://gitlab.com/evigorito/baseqtl_paper/-/blob/master/Snakefile).
The external data required to run this pipeline is deposited in
[Zenodo](https://doi.org/10.5281/zenodo.4759202). You will find 2
directories, ‘source\_data’ that contains the files to reproduce all
figures and tables and ‘fi\_data’ that takes files that dont need any
pre-processing step to reproduce the main data figures (see below).

### Preparing your Snakefile

Before running the pipeline you need to point to the directories with
the external data (source\_data and fig\_data) and the ouput directory
to generate your files.

  - You need to edit the following files:
    
    1.  Your configuration file, please edit [config
        file](https://gitlab.com/evigorito/baseqtl_paper/-/blob/master/config.yaml)
    2.  Your R scripts files in the folder
        [Scripts](https://gitlab.com/evigorito/baseqtl_paper/-/blob/master/Scripts)
        for calling the files in
        [Functions](https://gitlab.com/evigorito/baseqtl_paper/-/blob/master/Functions)

### Running your Snakefile

You first need to change your current directory to the directory where
your Snakefile is.

For running the whole pipeline, in the command line type:

    cd \path to Snakefile\
    # To generate a dry run to test the pipeline
    snakemake -np
    # To run the whole pipeline: the -j argument indicates the number of parallel jobs to run
    snakemake -j1

For running a specific rule:

1.  Comment out the rules you are not interetsted in (see rule all
    below).
2.  In the command line type:

<!-- end list -->

    cd \path to Snakefile\
    # To generate a dry run to test the pipeline
    snakemake -np -R Figure_3
    # To run the whole pipeline: 
    snakemake -j1 -R Figure_3

### The Snakefile

The Snakefile is structured as follows:

1.  Rule all: Each line corresponds to the output of a rule (in order).
    If you only want to run one rule, comment out the remainings rule.

2.  The rule power\_simulation provides the code to reproduce the
    simulations run to assess the loss of power of BaseQTL when
    genotypes are not available.

<!-- end list -->

  - Inputs files are listed below and can be found in the source\_data
    directory:
    1.  “neg.beta.prob.phasing.generalprior.eff2.stan” : Stan model for
        running BaseQTL with genotypes
    2.  “neg.beta.noGT2.rsnp.generalprior.eff2.stan” : Stan model for
        running BaseQTL without genotypes

<!-- end list -->

2.  The rule geu\_figs\_final provides the code and description of input
    files to reproduce the figures (main and supplemtary except
    diagrams) and tables using 86 of the subsamples of the GEUVADIS
    study
    [GUEVADIS\_sub](https://gitlab.com/evigorito/baseqtl_pipeline/-/blob/master/input/data/samples).

<!-- end list -->

  - Inputs files (source\_data directory):
    1.  “gene\_info.txt”, file with start and end positions for each
        genes, prepared in BaseQTL input pipeline
    2.  “GBR.samples”, file with the 86 sub-samples from GEUVADIS used
        in this study
    3.  “sample.downdate”, file with date when GEUVADIs samples were
        downloaded
    4.  “chr22.fSNPs.txt”, table with a list of fSNPs from chromosome
        22, output prepared in BaseQTL input pipeline from rule
        fSNP-gene
    5.  “HG96\_2215\_chr22\_q20.txt”, file with RNA-seq calls for the
        samples used in this study including read depth (DP) per sample
    6.  “fisher.fSNPs.txt”, table with columns “fsnp” “OR” “pvalue”
        “gene\_id” for all fSNPs run without genotypes, testing
        whether the frequency of heterozygous in study sample differs
        from reference panel by Fisher test (Methods).
    7.  “DNA.chr22.ASE.geuvadis\_sub.vcf.gz”, vcf file with ASE and
        genotype calls from DNA-seq, output from rule GT\_vcf in BaseQTL
        input pipeline
    8.  “RNA.chr22.ASE.geuvadis\_sub.vcf.gz”, vcf file with ASE and
        RNA-seq calls, outpur from RNA\_callVar in BaseQTL input
        pipeline
    9.  “DNA\_pre\_post\_AI.txt”, file with allelic counts for the fSNPs
        used with genotypes, prepared with rule AI\_post\_remapping in
        BaseQTL input pipeline
    10. “RNA\_pre\_post\_AI.txt”, file with allelic counts for the fSNPs
        used without genotypes, prepared with rule AI\_post\_remapping
        in BaseQTL input pipeline
    11. “GT\_fsnps\_inference.rds”, character vector with the fSNPs used
        in inference for BaseQTL with genotypes
    12. “Geuvadis\_sig\_eqtl”, from reference 23 in BaseQTL paper,
        significant eQTLs in 462 GEUVADIS samples
    13. “rasqual\_fdr.rds” ,file with RASQUAL output
    14. “lm\_fdr\_100KB.rds”, linear model output with analysis done for
        100KB cis-window
    15. “lm\_fdr\_500KB.rds”, linear model output with analysis done for
        500KB cis-window
    16. “baseqtl\_GT\_RNA\_NB.rds”, BaseQTL output with genotypes, no
        genotypes (RNA) and with genotypes but negative binomial model
    17. “baseqtl\_GT\_RNA\_sametags.rds”, eQTL effects for the same
        tagging cis-SNPs after running BaseQTL with or without genotypes
    18. “baseqtl\_noGT.rds”, output for BaseQTL without genotypes
        selecting cis-SNPs with info \>=0.3
    19. “baseqtl\_cisSNP\_gt\_error.rds”, BaseQTL output. Assess effect
        of genotyping error for eQTL calls. This QC runs fSNPs called
        from DNA-sequencing but masks genotypes for cis-SNP. Same run as
        noGT but uses vcf from GT and doesnt restrict minAseSnp, only
        with ref panel correction. Allows to compare false positives and
        false negatives due to cis-SNP imputation only (compare with GT)
    20. “baseqtl\_fSNP\_gt\_error.rds”, BaseQTL output. Assess effect of
        genotyping error for eQTL calls. This QC runs fSNPs called
        RNA-seq passing Fisher test and masks genotypes for cis-SNP.
        Same params as running noGT but uses vcf from GT, only run with
        ref panel correction. Allows to compare false positives and
        false negatives due to genotype error and missing values
        (comparison with noGT)
    21. “baseqtl\_fprior.rds” , BasQTL output with genotypes using a
        flat prior N(0,100)
    22. “baseqtl\_priors.rds”, BaseQTL output with genotypes with priors
        trained with external eQTL effects at different cis-windows from
        gene (1MB, 500KB and 100kB)
    23. “baseqtl\_gt\_input.rds”, inputs for runs with genotypes for the
        analysis of running time
    24. “table\_gt\_comp\_time.txt”, file with running time for BaseQTL
        with genotypes
    25. “table\_ngt\_comp\_time.txt”, files with running time for
        BaseQTL without genotypes

<!-- end list -->

3.  The rule nor\_pso\_final provides the code and description of input
    files to reproduce the main and supplementary figures for the eQTLs
    in normal and psoriasis skin.

<!-- end list -->

  - Inputs files (source\_data
        directory):
    1.  “Skin\_Sun\_Exposed\_Lower\_leg.v7.signif\_variant\_gene\_pairs.txt.gz”,
        file downloaded from
        [GTex](https://gtexportal.org/home/datasets)
    2.  “Skin\_Not\_Sun\_Exposed\_Suprapubic.v7.signif\_variant\_gene\_pairs.txt.gz”,
        file downloaded from
        [GTex](https://gtexportal.org/home/datasets)
    3.  “DRG\_JID2014.xls”, file with differentially regulated genes
        between [psoriasis and normal
        skin](https://ars.els-cdn.com/content/image/1-s2.0-S0022202X15368834-mmc2.%20xls)
    4.  “pso\_gwasSuppTable2.csv”, file with reported psoriasis gwas
        hits from reference 13 in BaseQTL paper
    5.  “psoriasis\_normal\_eQTL.rds”, BaseQTL output running psoriasis
        and normal skin in individual models
    6.  “psoriasis\_normal\_joint.rds”, BaseQTL output running psoriasis
        and normal skin in joint model
    7.  “fisher001EURr.rds”, file with fSNPs correlations based on EUR
        populations for all genes run
    8.  “fisher001EURsmallcis\_r.rds”, \# file with fSNPs correlations
        based on EUR populations for fSNPs in genes with significant
        associations
    9.  “r2\_spike\_gwas.txt”, file with columns “Gene\_id” “tag”
        “CHROM” “POS” “Alleles” “rs\_id” “gene\_dist” “table”
        “tag.pos” “r2TagGwas” with the r2 bweteen each tag and the
        closest psoriasis GWAS hits

<!-- end list -->

4.  Rules Figure 2-6:

These rules produce “pdf” files for each of the main figures except
diagrams. The inputs can be generated with rules “geu\_figs\_final” and
“nor\_pso\_final” and they are directly used to generate each panel.

  - Inputs files are in “fig\_data” directory.
    
    1.  “Fig2b.csv” table with the allelic counts for the fSNPs used for
        inference when running BaseQTL with genotypes.
    2.  “Fig2c.csv” table with the allelic imbalance estimates for the
        fSNPs used for inference when running BaseQTL with genotypes.
    3.  “Fig2c.csv” table with BaseQTL eQTL estimates with genotypes
        with or without reference panel bias corection.
    4.  “Fig3a.csv table with positive predictive value (PPV) and
        sensitivity for eGenes (100kB cis-window) for benchmarking
        BaseQTL
    5.  “Fig3b.csv table with positive predictive value (PPV) and
        sensitivity for eGenes (500kB cis-window) for benchmarking
        BaseQTL
    6.  “Fig3c.csv table with positive predictive value (PPV) and
        sensitivity for eQTLs (100kB cis-window) for benchmarking
        BaseQTL
    7.  “Fig3d.csv table with positive predictive value (PPV) and
        sensitivity for eQTLs (500kB cis-window) for benchmarking
        BaseQTL
    8.  “Fig4a.csv table with eQTL estimated by BaseQTL with or without
        genotypes in long format
    9.  “Fig4b.csv table with eQTL estimated by BaseQTL with or without
        genotypes running the same tags in both model (wide format)
    10. “Fig4c.rds list of genes with first element those significant
        with genotypes and second element without genotypes
    11. “Fig4d.rds list with eQTL effects estimated by BaseQTL, first
        element with genotypes and second without genotypes. Each
        element also contains the p-value observed in the GEUDAVIS study
        of 462 samples, when available.
    12. “Fig5a.rds” list with elements Normal, Both and Psoriasis. Each
        is a tablewith gene name and color to plot.
    13. “Fig5b.csv” table with data to make Sankeyplot.
    14. “Fig6.rds” list with 3 elements, the frist one is a table with
        eQTL effects for the top SNP for each of the selected genes
        (PI3, GSTP1 and SPRR1B) with individual or joint model. Second
        element is a table with expected genotypes for the eQTL and
        total gene counts adjusted by library size for each gene-SNP
        test and each individual. The 3rd element is a table for the
        heterozygous individuals with the allelic fold change for each
        gene-SNP association.
