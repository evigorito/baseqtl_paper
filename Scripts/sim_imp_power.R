library(data.table)
library(rstan)
library(parallel)
library(Matrix)


rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

## source('../Functions/sim_functions.R') # adjust path to call this file
source("/home/ev250//myPipelines/baseqtl_paper/Functions/sim_functions.R")


##' Simulate  an EQTL effect for a sample and simulate a reference panel. PErform inference with and without genotypes
##'
##' @param H number of haplotypes to simulate for population
##' @param S number of simulations
##' @param N number of ind reference panel
##' @param s number of individuals to sample
##' @param f number of fSNPs
##' @param maf allel freq for cis SNP and fSNPs (in this order)
##' @param cov covariance betwen SNPs
##' @param phi overdispersion for negative binomial
##' @param theta overdispersion for beta binomial
##' @param mu mean total gene expression
##' @param frac_ase fraction of total counts that corresponds to ase counts
##' @param p proportion of allelic imbalance relative to alternative allele
##' @param simulation_id identifier for simulation to add to results summary
##' @param prior list with mean, sd and mix proportions to use for eQTL effect prior, defaults to BaseQTL prior
##' @param model_gt Stan model to use for known genotypes
##' @param model_nogt model to use with unknown genotypes
##' @param probs quantile for posterior distribution to extract from stan, defaults to 0.99 and 0.95
##' @param out dir to save output
##' sim_run()

sim_run <- function(H, S, N, s, f, maf, cov, phi, theta, mu, frac_ase, p, simulation_id, prior=NULL, model_gt, model_nogt, probs=NULL, out){

    if(is.null(prior)){
    ## use default prior
        prior=c(0,0, 0.0309, 0.3479, 0.97359164, 0.02640836)
        k=length(prior)/3 ## number of gaussians
        s <- seq(1,length(prior),k)
        l <- lapply(1:3, function(i) as.numeric(prior[s[i]: (s[i]+k-1)]))
        names(l) <- c("mean", "sd", "mix")
        prior <- l
    }
    if(is.null(probs)) probs <-  c(0.005, 0.025, 0.25, 0.50, 0.75, 0.975, 0.995)
    
#### Simulation of population  

    hm1 <- sim.pop.haps(f=f,m=maf,cov=cov,N=H/2,r2=TRUE,maf=TRUE) # N number of individuals in population
    
    print("hm1 maf and r2")
    print(hm1$maf)
    print(hm1$r2)

    ## Save inputs and results

    lapply(c('maf', 'r2'), function(i) saveRDS(hm1[[i]], paste0(out, "/", i, "_", simulation_id, ".rds")))
    

    ##sampling reference panel

    h1 <- lapply(1:S, function(i) hm1[[1]][sample(1:nrow(hm1[[1]]),size=N,replace=TRUE),])
    h2 <- lapply(1:S, function(i) hm1[[1]][sample(1:nrow(hm1[[1]]),size=N,replace=TRUE),])
    
    ## sampling samples

    s1 <- lapply(1:S, function(i) hm1[[1]][sample(1:nrow(hm1[[1]]),size=n,replace=TRUE),])
    s2 <- lapply(1:S, function(i) hm1[[1]][sample(1:nrow(hm1[[1]]),size=n,replace=TRUE),])

    ## phased haplotyes for cis and fsnps in samples

    samp.haps  <- lapply(1:S, function(s) {
        dt <- data.table()
        dt[, paste0("snp.", 1:(f+1)) := lapply(1:(f+1), function(i) paste(s1[[s]][,i], s2[[s]][,i], sep="|"))]
        dt <- as.data.table(t(dt))
        names(dt) <- paste0("id", 1:ncol(dt), "_GT")
        return(dt)
    }
    )
    ## fsnps
    f.ase <- lapply(samp.haps, function(s) s[2:(f+1), ])
    f.ase.r <- rec_mytrecase_rSNPs(x=1:S, y= rbindlist(f.ase)[,POS:=1:(S*f)])

    ## phased haplotypes for regulatory SNP

    rs.hap <- lapply(samp.haps, function(s) s[1,])

    rec.rs <- rec_mytrecase_rSNPs(x=1:S, y=rbindlist(rs.hap)[, POS:=1:S])

    
    ## combine recoded  fSNPs with rsnp for each simulation

    rec.all <- lapply(1:S, function(i) {
        mat <- t(rbind(f.ase.r[i:(i+(f-1)),], rec.rs[i,]))
        mat <- mat[rownames(mat) != "POS",]
    })

    ## expression in samples
    counts.ase <- lapply(1:S, function(i) {
        dt <- tot.ase(f=f,g=rec.all[[i]],mu=mu,phi=phi,theta=theta,p=p,f.ase=frac_ase)
        mat <- as.matrix(dt)
        rownames(mat) <- paste0('id', 1:nrow(mat))
        return(mat)
    }
    )

    rp <- lapply(1:S, function(i) {
        tmp <- matrix(NA, nrow= f+1, ncol =2*nrow(h1[[1]]))
        for(r in 1:(f+1)){
            tmp[r, seq(1, ncol(tmp), 2)] <- h1[[i]][,r]
            tmp[r, seq(2, ncol(tmp), 2)] <- h2[[i]][, r]
        }
        return(tmp)
    })

    rp.f <- lapply(1:S, function(s) {
        mat <- rp[[s]][2:(f+1),]
        rownames(mat) <- paste0("fsnp.", 1:f)
        return(mat)
    })

    
    ## Get total counts

    counts <- lapply(1:S, function(i) counts.ase[[i]][,"y"])

    
    ## Prepare Stan inputs
    stan.f <- lapply(1:S, function(s) stan.fsnp.noGT.eff(rp.f[[s]],f.ase[[s]],counts.ase[[s]], NB="no"))

    
    
    stan.in1 <- lapply(1:100, function(s) stan.trecase.eff2(counts[[s]], rp.1r=rp[[s]][1,,drop=FALSE], rp.f[[s]], f.ase[[s]], rs.hap=rs.hap[[s]], rec.rsnp=copy(rec.rs[s,]), stan.f[[s]]))
    
    keep <- unlist(lapply(stan.in1, is.list))

    stan.in1 <- stan.in1[keep]

    ## Save for QC
    saveRDS(stan.in1, paste0(out, "/", simulation_id, "_inputGT.rds"))
   
    stan.in2 <- lapply(stan.in1, function(i) {
        l <- in.neg.beta.prob.eff2(i)
        ## add prior
        return(add.prior(prior, l))
    }      
    )

    ## count number of hets with sufficient ase counts to input in final report
    ASE.hets <- sapply(stan.in1, function(i) nrow(i$gm[abs(g.ase)==1,]))

    
    nhet <- apply(rec.rs[keep, names(rec.rs) != "POS", with=F], 1, function(i) sum(abs(i)==1))

    ## Run Stan with genotypes

    message("Running Stan with genotypes")

    mod <- stan_model(model_gt)

    stan.full <- mclapply(stan.in2, function(i) run.stan(mod, i, 'bj', probs))

    names(stan.full) <- 1:length(stan.full)
    full.sum <- stan.bt(x=stan.full,y= NULL, rtag=NULL, model="NB_ASE", nhets=nhet, ASE.het=ASE.hets,gene=simulation_id, EAF=data.table(snp =names(stan.full), eaf=hm1$maf[1]), nfsnps=f, probs=probs )
    

    ## Run Stan but with cis-SNP imputation

    ## First assume fSNPs genotypes are known and correct in all samples

    counts.g <- lapply(counts, function(c)  as.data.table(matrix(c, nrow=1, dimnames=list(NULL, names(c)))))

    inp <- lapply(1:S, function(s) aux.in3(simulation_id, ai=NULL, case=counts.ase[[s]], rp.f[[s]], rp[[s]][1,,drop=F], f.ase[[s]][, id:=paste0('fsnp.', 1:f)], counts.g[[s]], covariates=1, min.ase=5, min.ase.n=5, info=0, snps.ex=data.table(), prefix=NULL, out=NULL))

    ## Save for QC
    saveRDS(inp, paste0(out, "/", simulation_id, "_inputNoGT.rds"))

    ## add prior

    stan.noGT2 <- lapply(inp, function(i) add.prior(prior, i$stan.noGT2[[1]]))

    info.ok <- unlist(lapply(inp, function(i) i$info.ok))

    ## Run Stan

    message("Running Stan without genotypes")

    mod2 <- stan_model(model_nogt)

    stan.full2 <-  mclapply(stan.noGT2, function(i) run.stan(mod2, i, 'bj', probs))
            
    names(stan.full2) <- 1:length(stan.full2)

    full.sum2 <- stan.bt(x=stan.full2,y= NULL, rtag=NULL, model="NB_ASE", nhets=nhet, ASE.het=ASE.hets,gene=simulation_id, EAF=data.table(snp =names(stan.full2), eaf=hm1$maf[1]), nfsnps=f, probs=probs, info=setNames(info.ok,names(stan.full2) ))

    
    write.table(full.sum, paste0(out, "/", simulation_id, "_GT.txt"), row.names=FALSE)
    write.table(full.sum2, paste0(out, "/", simulation_id, "_noGT.txt"), row.names=FALSE)
    
}




## Calling function sim_run

## Reading Snakefile arguments

H <- as.numeric(snakemake@params[['H']]) # number of haplotypes to simulate for population
S <- as.numeric(snakemake@params[['S']]) # number of simulations
N <- as.numeric(snakemake@params[['N']]) # number of ind reference panel
n <- as.numeric(snakemake@params[['n']]) # number of individuals to sample

maf <- snakemake@params[['mean']]
cov <- as.numeric(snakemake@params[['cov']])
phi <- as.numeric(snakemake@params[['negbinom_overdispersion']])
theta <- as.numeric(snakemake@params[['bb_overdispersion']])
mu <- as.numeric(snakemake@params[['mu']]) # mean gene expression
p <- as.numeric(snakemake@params[['eQTL_effect_p']])
frac_ase <- as.numeric(snakemake@params[['frac_ase']])
model_gt <- snakemake@input[['model_gt']]
model_nogt <- snakemake@input[['model_nogt']]
out <- snakemake@params[['out']]

simulation_id <- snakemake@params[['prefix']]

f <- length(maf) -1


sim_run(H, S, N, s, f, maf, cov, phi, theta, mu, frac_ase, p, simulation_id, prior=NULL, model_gt, model_nogt, probs=NULL, out)
