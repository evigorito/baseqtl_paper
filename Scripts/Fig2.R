source("/home/ev250/myPipelines/baseqtl_paper/Functions/make_figures.R") # adjust path to source figures file from Functions directory

#################################################
########## Get files from Snakemake ###########################
#################################################

fig2b_f <- snakemake@input[['fig2b']]
fig2c_f <- snakemake@input[['fig2c']]
fig2d_f <- snakemake@input[['fig2d']]

out <- snakemake@output[['out']]


##################################
## Fig 2b
##################################

AI.hets <- fread(fig2b_f)

AI.hets.p <- ggplot(AI.hets[ Inference=='Yes',], aes( log(depth),qlogis(raw.AI) )) +
    geom_point(shape=1, color="blue") +
    geom_vline(xintercept=log(100), linetype="dashed", color="gray") +
    geom_hline(yintercept=0, linetype="dashed") +
    ylab("AI (raw estimate)") +
    xlab("Depth (log)") +
    scale_y_continuous(breaks=c(-3,0,3,6), limits=c(-4,7)) +
    scale_x_continuous(breaks=seq(6,12,2), limits=c(1, 14)) +
    geom_rug(col=rgb(.3,.5,.5,alpha=.05)) +
    theme_bw()


##################################
## Fig 2c
##################################

AI <- fread(fig2b_f)

EAI <- ggplot(AI[Total_post>0 & Inference == "Yes" ,] ) +
    geom_line(aes(x=log(Total_pre), y=qlogis(null_hCI)),  colour="gray", linetype="dashed", na.rm=TRUE) +
    geom_point(aes(x=log(Total_pre), y=qlogis(AI_post.plot)), shape=1, color="blue") +
    geom_vline(xintercept=log(100), linetype="dashed") +
    geom_hline(yintercept=0, linetype="dashed") +
    ylab("Reference bias estimate") +
    xlab("Depth (log)") +
    scale_y_continuous(breaks=c(-3,0,3,6), limits=c(-4,7)) +
    scale_x_continuous(breaks=seq(6,12,2), limits=c(1, 14)) +
    geom_rug(aes(x=log(Total_pre), y=qlogis(AI_post.plot)), color="blue", alpha=0.1) + 
    theme(strip.background =element_rect(fill="white", color="black", linetype="solid")) +
    theme_bw()


##################################
## Fig 2d
##################################

ref.bias <- fread(fig2d_f)

## select associations within 100 kB
rbias=c("norefbias", "rbias")
cols <- c("log2_aFC_mean", "log2_aFC_0.5%", "log2_aFC_99.5%", "null.99")
colors.p <- c("#999999", "green3", "orchid4", "salmon")

d<- 10^5
dt <- ref.bias[abs(gene.dist) <=d,][, Signif := as.factor(Signif)]

tab <- tab2bplot(dt, colors=setNames(colors.p, levels(dt$Signif)))


k=c(-1.0, -.4, .5, 1)
j="Observed-GT"
       
rbias.p <- btrecase.plot(dt[Signif !="None" ,],
                       x1=paste(cols, rbias[1], sep="."),
                       x2= paste(cols, rbias[2], sep="."),
                       xl='eQTL-effect (no correction)', yl='eQTL-effect\n(correction)',
                       col=c("Without", "With"),axis.title=12, axis.text=10,
                       legend.title=12, legend.text=10,
                       legend.symbol=4, point.size=3 ,
                       colors=colors.p) +
    annotation_custom(tableGrob(tab[,.(Signif,SNPs)],
                                rows=NULL,
                                theme=ttheme_minimal(base_size = 10,padding = unit(c(2, 1.5), "mm"),
                                                     core=list(fg_params=list(col=c(tab$color, rep("black", 4)))))),
                      xmin=k[1], xmax=k[2], ymin=k[3], ymax=k[4])

## Make figure and save

AI.est.gt <- plot_grid(AI.hets.p, EAI, rbias.p,  nrow=1,  rel_widths=c(0.8, 0.8, 1))

ggsave(out, AI.est.gt, width = 12, height = 3)                    
