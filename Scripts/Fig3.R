source("/home/ev250/myPipelines/baseqtl_paper/Functions/make_figures.R") # adjust path to source figures file from Functions directory

#################################################
########## Get files from Snakemake #############
#################################################


l <- list(a = snakemake@input[['fig3a']],
b = snakemake@input[['fig3b']],
c = snakemake@input[['fig3c']],
d =snakemake@input[['fig3d']]
)

out <- snakemake@output[['out']]

######################################
########## Make figures  #############
######################################

data <- lapply(l, fread)


plots <- mapply(function(i,j) fdr.plot(dt=i,x="PPV",
                                           y="Sensitivity",
                                           lab.col="N.total",
                                           type=j,
                                           col="sig.gs",
                                           subtitle= paste("Total associations:", unique(i$Total_assoc)),
                                           path="no",
                                       legend.in="yes"),
                i=data,
                j=rep(c("eGenes", "assoc"), each=2),
                SIMPLIFY=F)

all.fdr <- plot_grid(plotlist=plots, labels ="auto", nrow=2)


ggsave(out, all.fdr, width = 7.8, height = 7)
